# SOBRE O PROJETO Desenvolvido

# PLATAFORMA VERDE

* **Projeto desenvolvido,** para o Processo Seletivo na *Plataforma Verde*



2 Aplicações, sendo elas:  

- 1. Interface de usuário para consumir essa API ( não exigido )
- 2. API PlataformaVerde - API RESTFull

Utilizando:  

- Laravel última versão
- Queue para processar a planilha.
- REST
- JSON

1 - Interface para Importar a planílha
<br/>
# URL
http://127.0.0.1:8000/importar

2 - API RESTFull:  
<br/>
# METODOS DA  API:
## OBTER RESÍDUOS
----
  Retorna todos os resídulos em formato json.

* **URL**
http://127.0.0.1:8000/api/PlataformaVerde
* **Method:**
   `GET`

*  **URL Params**
    None

* **Data Params**
   None

* **Success Response:**
   * **Code:** 200 
    **Content:** `{ 
            "status":"success", 
            "residuos": [{
                "id":
                "nome_comum": 
                "tipo":
                "categoria":
                "tecnologia_tratamento":
                "classe":
                "unidade_medida":
                "peso":
            }]
            }`
 
* **Error Response:**
  * **Code:** 404 NOT FOUND   
    **Content:** `{ "status" : "error", "message":"Nenhum residuo cadastrado!" }`
   
   OR

  * **Code:** 500 Internal Server Error   
    **Content:** `{ "status" : "error", "message":"Erro inesperado, por favor tente novamente!" }`


## IMPORTAR UMA NOVA PLANÍLHA
----
* **URL**
http://127.0.0.1:8000/api/PlataformaVerde

* **Method:**
   `POST`

*  **URL Params**
   None

* **Data Params**
    **input type="file"**
        "planilha": 
    ```

* **Success Response:**

   * **Code:** 200 
    **Content:** `{ 
            "status":"success", 
            "residuos": [{
                "id":
                "nome_comum": 
                "tipo":
                "categoria":
                "tecnologia_tratamento":
                "classe":
                "unidade_medida":
                "peso":
            }]
            }`
* **Error Response:**
 
  * **Code:** 500 Internal Server Error   
    **Content:** `{ "status" : "error", "message":"Erro inesperado, por favor tente novamente!" }`

## IMPORTAR UMA PLANÍLHA ATUALIZADA!
----
* **URL**
http://127.0.0.1:8000/api/PlataformaVerde

* **Method:**
   `PUT` ou `POST` ( se enviar via POST, o sistema verificará se já existe e faz a atualização.)

*  **URL Params**
   None

* **Data Params**
    **input type="file"**
        "planilha": 
    ```

* **Success Response:**

   * **Code:** 200 
    **Content:** `{ 
            "status":"success", 
            "residuos": [{
                "id":
                "nome_comum": 
                "tipo":
                "categoria":
                "tecnologia_tratamento":
                "classe":
                "unidade_medida":
                "peso":
            }]
            }`
* **Error Response:**
  * **Code:** 404 NOT FOUND   
    **Content:** `{ "status" : "error", "message":"Nenhum residuo cadastrado!" }`

  OR

  * **Code:** 500 Internal Server Error   
    **Content:** `{ "status" : "error", "message":"Erro inesperado, por favor tente novamente!" }`

## OBTER UM RESÍDUO ESPECÍFICO
----
  retorna os dados de um resíduo específico, de acordo com o ID mencionado.

* **URL**
http://127.0.0.1:8000/api/PlataformaVerde/:id

* **Method:**
 
  `GET`

*  **URL Params**
   **Required:**
     `id=[integer]`

* **Data Params**
   None

* **Success Response:**
  * **Code:** 200  
    **Content:** `{ 
            "status":"success", 
            "residuos": {
                "id":
                "nome_comum": 
                "tipo":
                "categoria":
                "tecnologia_tratamento":
                "classe":
                "unidade_medida":
                "peso":
            }
            }`
 
* **Error Response:**

  * **Code:** 404 NOT FOUND   
    **Content:** `{ "status" : "error", "message":"Nenhum residuo cadastrado!" }`

  OR

  * **Code:** 500 Internal Server Error   
    **Content:** `{ "status" : "error", "message":"Erro inesperado, por favor tente novamente!" }`

## EXCLUIR UM RESÍDUO ESPECÌFICO
----
  Exclui um resíduo específico, de acordo com o ID mencionado.

* **URL**
http://127.0.0.1:8000/api/PlataformaVerde/:id

* **Method:**
 
  `DELETE`

*  **URL Params**
   **Required:**
     `id=[integer]`

* **Data Params**
   None

* **Success Response:**
  * **Code:** 200  
    **Content:**`{ "status" : "success", "message":"Residuo excluido com sucesso!" }`
 
* **Error Response:**

  * **Code:** 404 NOT FOUND   
    **Content:** `{ "status" : "error", "message":"Nenhum residuo encontrado com este ID!" }`

  OR

  * **Code:** 500 Internal Server Error   
    **Content:** `{ "status" : "error", "message":"Erro inesperado, por favor tente novamente!" }`


# Meu WhatsApp: *11 - 9.5680-4182* , Raphael Renan.
