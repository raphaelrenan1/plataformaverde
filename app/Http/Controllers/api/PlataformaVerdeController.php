<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Excel;
use App\PlataformaVerde;
use App\Imports\ImportarResiduoController;
use Response;


class PlataformaVerdeController extends Controller
{

    public function tela_importar()
    {
      $data = DB::table('plataforma_verdes')->orderBy('id', 'DESC')->get();
      return view('importar', compact('data'));
    }
    public function index()
    {
        try {
            //retorna todos os resíduos já importados em json e código de retorno http 200-ok
            $ImportadoSucesso = PlataformaVerde::all();
            if (!$ImportadoSucesso->isEmpty()) {
                return Response::json(
                    [
                        'status'=>'success',
                        'residuos'=>PlataformaVerde::all()
                    ],
                    200
                );
            }
            return Response::json(
                [
                'status'=>'error',
                'message'=>'Nenhum residuo cadastrado!'
                ],
                404
            );
        } catch (Exception $exception) {
            return Response::json(
                [
                    'status'=>'error',
                    'message'=>'Erro inesperado, por favor tente novamente!'
                ],
                500
            );
        }

    }

    // só é possível criar novos produtos via planilha. então não criaremos via POST.
    public function store(Request $request)
    {
     $this->validate($request, [
      'planilha'  => 'required|mimes:xls,xlsx'
     ]);

     $path = $request->file('planilha')->getRealPath();

      //só é possível, cadastrar via planílha !
        try {
            // Importar usando Queue, para processar
            $ImportadoSucesso = Excel::queueImport(new ImportarResiduoController, request()->file('planilha'));
            if ($ImportadoSucesso) {

                return Response::json(
                    [
                        'status'=>'success',
                        'residuos'=>'Planílha processada com sucesso!'//processando queue -> PlataformaVerde::all()
                    ],
                    200
                );
            }
            return Response::json(
                [
                'status'=>'error',
                'message'=>'Nenhum residuo cadastrado!'
                ],
                404
            );
        } catch (Exception $exception) {
            return Response::json(
                [
                    'status'=>'error',
                    'message'=>'Erro inesperado, por favor tente novamente!'
                ],
                500
            );
        }    
  }

    public function show($id)
    {
        //visualizar
        //retorna os dados do resíduo citado em: /api/PlataformaVerde/1 , onde 1 eh o id.
        //return PlataformaVerde::findOrFail($id);

        try {
            // exibir apenas o resíduo mencionado em :id
            $ImportadoSucesso = PlataformaVerde::findOrFail($id);
            if ($ImportadoSucesso) {

                return Response::json(
                    [
                        'status'=>'success',
                        'residuos'=>PlataformaVerde::findOrFail($id)
                    ],
                    200
                );
            }
            return Response::json(
                [
                'status'=>'error',
                'message'=>'Nenhum residuo cadastrado!'
                ],
                404
            );
        } catch (Exception $exception) {
            return Response::json(
                [
                    'status'=>'error',
                    'message'=>'Erro inesperado, por favor tente novamente!'
                ],
                500
            );
        }         
    }

    public function update(Request $request, $id)
    {
        //atualizar
        //só é possível atualizar, via planílha !
        try {
     $this->validate($request, [
      'planilha'  => 'required|mimes:xls,xlsx'
     ]);

     $path = $request->file('planilha')->getRealPath();          
            // Importar usando Queue, para processar
            $ImportadoSucesso = Excel::queueImport(new ImportarResiduoController, request()->file('planilha'));
            if ($ImportadoSucesso) {

                return Response::json(
                    [
                        'status'=>'success',
                        'residuos'=>PlataformaVerde::all()
                    ],
                    200
                );
            }
            return Response::json(
                [
                'status'=>'error',
                'message'=>'Nenhum residuo cadastrado!'
                ],
                404
            );
        } catch (Exception $exception) {
            return Response::json(
                [
                    'status'=>'error',
                    'message'=>'Erro inesperado, por favor tente novamente!'
                ],
                500
            );
        }    
    }

    public function destroy($id)
    {
       //apagar
        try {
        //permite apagar um resídulo conforme seu id.
        $PlataformaVerde = PlataformaVerde::findOrFail($id);
        //linha abaixo, uso para validar se encontrou o :id
        $ImportadoSucesso = PlataformaVerde::findOrFail($id);

            if ($ImportadoSucesso) {
        //Então exclua o resíduo mencionado em :id      
        $PlataformaVerde->delete();              

                return Response::json(
                    [
                        'status'=>'success',
                        'message'=>'Residuo excluido com sucesso!'
                    ],
                    200
                );
            }
            return Response::json(
                [
                'status'=>'error',
                'message'=>'Nenhum residuo encontrado com este ID!'
                ],
                404
            );
        } catch (Exception $exception) {
            return Response::json(
                [
                    'status'=>'error',
                    'message'=>'Erro inesperado, por favor tente novamente!'
                ],
                500
            );
        }           
    }
}
