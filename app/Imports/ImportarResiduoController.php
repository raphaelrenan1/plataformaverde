<?php

namespace App\Imports;

use App\PlataformaVerde;
use Maatwebsite\Excel\Concerns\ToModel;
//para processar usando queue, linha ABAIXO
use Illuminate\Contracts\Queue\ShouldQueue;
//para processar usando queue, linha ACIMA
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Response;

class ImportarResiduoController implements WithChunkReading, ShouldQueue, WithStartRow, OnEachRow
{  
    public function onRow(Row $row)
    {
        $row = $row->toArray();        
        try {


        $PlataformaVerde = PlataformaVerde::firstOrCreate(
            ['nome_comum'     => $row[1]], // Verificar pelo nome do resídulo se já existe.
            [
            'nome_comum'     => $row[1],
            'tipo'     => $row[2],
            'categoria'     => $row[3],
            'tecnologia_tratamento'     => $row[4],
            'classe'     => $row[5],
            'unidade_medida'     => $row[6],
            'peso'     => $row[7],
            ]

        );      
        if (! $PlataformaVerde->wasRecentlyCreated) {
            $PlataformaVerde->update([
            'tipo'     => $row[2],
            'categoria'     => $row[3],
            'tecnologia_tratamento'     => $row[4],
            'classe'     => $row[5],
            'unidade_medida'     => $row[6],
            'peso'     => $row[7],
            ]);
        }            
            // Importar usando Queue, para processar
            $ImportadoSucesso = $PlataformaVerde;
            if ($ImportadoSucesso) {

                return Response::json(
                    [
                        'status'=>'success',
                        'residuos'=>PlataformaVerde::all()
                    ],
                    200
                );
            }
            return Response::json(
                [
                'status'=>'error',
                'message'=>'Nenhum residuo cadastrado!'
                ],
                404
            );
        } catch (Exception $e) {
            return Response::json(
                [
                    'status'=>'error',
                    'message'=>'Erro inesperado, por favor tente novamente!'
                ],
                500
            );
        }            

    }

    public function startRow(): int
    {
        return 6;
    }    

    public function chunkSize(): int
    {
        //A cada 500 linhas, será executado em um trabalho de fila./queue
        return 500;
    }    
}
