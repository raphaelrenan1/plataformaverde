<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class PlataformaVerde extends Model
{
    use Notifiable;	
    //
    protected $fillable = [
        'nome_comum', 'tipo', 'categoria','tecnologia_tratamento','classe','unidade_medida','peso'
    ];    
}
