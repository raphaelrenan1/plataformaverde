<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlataformaVerdesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plataforma_verdes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_comum');
            $table->string('tipo');
            $table->string('categoria');
            $table->string('tecnologia_tratamento');
            $table->string('classe');
            $table->string('unidade_medida');
            $table->string('peso');
            $table->timestamps('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plataforma_verdes');
    }
}
