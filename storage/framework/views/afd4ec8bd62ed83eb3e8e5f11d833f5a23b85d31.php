<!DOCTYPE html>
<html>
 <head>
  <title>Interface Para Importar a planílha ao Sistema</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 </head>
 <body>
  <br />
  
  <div class="container">
   <h3 align="center">Seja Bem-vindo!</h3>
    <br />
   <?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
     Erro na validação do Importação de Planílha<br><br>
     <ul>
      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li><?php echo e($error); ?></li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
     </ul>
    </div>
   <?php endif; ?>

   <?php if($message = Session::get('success')): ?>
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong><?php echo e($message); ?></strong>
   </div>
   <?php endif; ?>
   <form method="post" enctype="multipart/form-data" action="/api/PlataformaVerde">
    <?php echo e(csrf_field()); ?>

    <div class="form-group">
     <table class="table">
      <tr>
       <td width="40%" align="right"><label>Selecione a Planílha do Excel para Importar</label></td>
       <td width="30">
        <input type="file" accept=".xls,.xlsx"  name="planilha" />
       </td>
       <td width="30%" align="left">
        <input type="submit" name="upload" class="btn btn-primary" value="Importar">
       </td>
      </tr>
      <tr>
       <td width="40%" align="right"></td>
       <td width="30"><span class="text-muted">.xls, .xslx</span></td>
       <td width="30%" align="left"></td>
      </tr>
     </table>
    </div>
   </form>
   
   <br />
   <div class="panel panel-default">
    <div class="panel-heading">
     <h3 class="panel-title">Resíduos já importados</h3>
    </div>
    <div class="panel-body">
     <div class="table-responsive">
      <table class="table table-bordered table-striped">
       <tr>
        <th>Nome Comum do Resíduo</th>
        <th>Tipo do Resíduo</th>
        <th>Categoria</th>
        <th>Tecnologia de Tratamento</th>
        <th>Classe</th>
        <th>Unidade de Medida</th>
        <th>Peso</th>
       </tr>
       <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
       <tr>
        <td><?php echo e($row->nome_comum); ?></td>
        <td><?php echo e($row->tipo); ?></td>
        <td><?php echo e($row->categoria); ?></td>
        <td><?php echo e($row->tecnologia_tratamento); ?></td>
        <td><?php echo e($row->classe); ?></td>
        <td><?php echo e($row->unidade_medida); ?></td>
        <td><?php echo e($row->peso); ?></td>
       </tr>
       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </table>
     </div>
    </div>
   </div>
  </div>
 </body>
</html>